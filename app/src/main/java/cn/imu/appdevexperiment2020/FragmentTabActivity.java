package cn.imu.appdevexperiment2020;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class FragmentTabActivity extends AppCompatActivity implements View.OnClickListener {

    public String[] btnTitles = new String[]{" 消息 "," 小世界 ", " 联系人 "," 动态 "};
    public List<Fragment> contextFragments = new ArrayList<>();
    public LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_tab);
        init();
    }

    private void init() {
        initButton();
        initFragment();
    }

    public void initButton() {
        linearLayout = findViewById(R.id.buttonLayout);
        for (String btnStr: btnTitles) {
            Button btn = new Button(this);
            btn.setText(btnStr);
            btn.setTag(btnStr);
            btn.setBackgroundColor(Color.WHITE);
            LinearLayout.LayoutParams btnLayoutParams =
                    new LinearLayout.LayoutParams
                            (0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
            btn.setOnClickListener(this);
            linearLayout.addView(btn, btnLayoutParams);
        }
    }

    public void initFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        for (String btnStr: btnTitles) {
            ContextFragment contextFragment = new ContextFragment();
            transaction.add(R.id.contextFrameLayout, contextFragment, btnStr);
            Bundle bundle = new Bundle();
            bundle.putString("textValue", btnStr);
            contextFragment.setArguments(bundle);
            contextFragments.add(contextFragment);
        }
        transaction.commit();
        showFragment(btnTitles[0]);
    }

    public void showFragment(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        for (Fragment fragment: contextFragments) {
            if (fragment.getTag().equals(tag)) {
                transaction.show(fragment);
            } else {
                transaction.hide(fragment);
            }
        }
        transaction.commit();
    }

    @Override
    public void onClick(View view) {
        showFragment(view.getTag().toString());
    }
}
