package cn.imu.appdevexperiment2020;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class Database extends SQLiteOpenHelper {
    private SQLiteDatabase db;
    public static final String CREATE_User = "create table qquser ("
            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "name TEXT, "
            + "password TEXT,"
            + "sex TEXT)";
    public Database(@Nullable Context context) {
        super(context, "db_test", null, 1);
        db = getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_User);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS qquser");
        onCreate(db);
    }

    public void add(String name,String password,String sex){
        db.execSQL("INSERT INTO user(name,password,sex)VALUES(?,?,?)",new Object[]{name,password,sex});
    }

    public ArrayList<SQLite> getAllDATA(){
        ArrayList<SQLite> list = new ArrayList<SQLite>();
        Cursor cursor = db.query("qquser",null,null,null,null,null,"name DESC");
        while(cursor.moveToNext()){
            @SuppressLint("Range") String name = cursor.getString(cursor.getColumnIndex("name"));
            @SuppressLint("Range") String password = cursor.getString(cursor.getColumnIndex("password"));
            @SuppressLint("Range") String sex =cursor.getString(cursor.getColumnIndex("sex"));
            list.add(new SQLite(name,password,sex));
        }
        return list;
    }
}
