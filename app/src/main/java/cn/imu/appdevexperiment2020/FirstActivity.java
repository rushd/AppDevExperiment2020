package cn.imu.appdevexperiment2020;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class FirstActivity extends BaseActivity {

    /*
    点击菜单显示menu1，menu2
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    /*
    点击按钮menu1，menu2弹出Toast提醒
    */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu1_item:
                Toast.makeText(this,"你点击了menu1",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu2_item:
                Toast.makeText(this,"你点击了menu2",
                        Toast.LENGTH_SHORT).show();
                break;
            default:
        }
        return true;
    }

    /*
    点击按钮弹出Toast提醒.
    Toast.makeText需要传入3个参数，
    （this（context对象），“提醒内容”（text），Toast.LENGTH_SHORT（显示时长）），
    再调用show（）显示出来
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Log.d("FirstActivity",this.toString());*/

        /* singleInstance启动模式打印日志*/
        Log.d("FirstActivity","Task id is " + getTaskId());

        setContentView(R.layout.first_layout);
        Button button1 = (Button) findViewById(R.id.button_1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Toast提醒
                Toast.makeText(FirstActivity.this, "跳转中",
                        Toast.LENGTH_SHORT).show();
                */

                /* 显式Intent传递数据*//*
                String data = "这是第二个活动";
                Intent intent = new Intent(FirstActivity.this,
                        SecondActivity.class);
                intent.putExtra("extra_data",data);
                *//*startActivity(intent);*//*


                *//* 返回数据给上一个活动*//*
                startActivityForResult(intent,1);*/


                /* 隐式Intent
                Intent intent = new Intent("com.example.activitytest.ACTION_START");
                intent.addCategory("com.example.activitytest.MY_CATEGORY");
                startActivity(intent);
                */

                /* 调用内置浏览器
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.baidu.com"));
                startActivity(intent);
                */

                /* standard启动模式*//*
                Intent intent = new Intent(FirstActivity.this,
                        FirstActivity.class);
                startActivity(intent);*/

                /* 页面按钮跳转*/
                /*Intent intent = new Intent(FirstActivity.this,
                        SecondActivity.class);
                startActivity(intent);*/

                /* 启动SecondActivity最佳写法*/
                SecondActivity.actionStart(FirstActivity.this,
                        "data1","data2");

            }
        });
    }

    /* 重写onActivityResult方法来得到返回的数据*/
    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    String returnedData = data.getStringExtra("data_return");
                    Log.d("FirstActivity", returnedData);
                }
                break;
            default:
        }
    }*/

    /* singleTask启动模式中的onRestart()方法*/
    @Override
    protected void onRestart(){
        super.onRestart();
        Log.d("FirstActivity", "onRestart");
    }

}