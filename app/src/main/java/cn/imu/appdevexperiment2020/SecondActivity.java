package cn.imu.appdevexperiment2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Log.d("SecondActivity",this.toString());*/
        Log.d("SecondActivity","Task id is " + getTaskId());
        setContentView(R.layout.second_layout);

        /* 显式传递参数*//*
        Intent intent = getIntent();
        String data = intent.getStringExtra("extra_data");
        Log.d("SecondActivity",data);


        *//* 返回数据给上一个活动*//*
        Button button2 = (Button) findViewById(R.id.button_2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("data_return","这是第一个活动");
                setResult(RESULT_OK,intent);
                finish();
            }
        });*/

        /*Button button2 = (Button) findViewById(R.id.button_2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this,
                        FirstActivity.class);
                startActivity(intent);
            }
        });*/

        /* singleInstance启动模式*/
        Button button2 = (Button) findViewById(R.id.button_2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this,
                        ThirdActivity.class);
                startActivity(intent);
            }
        });

    }
    /* back返回键返回数据*/
    @Override
    public void onBackPressed(){
        Intent intent = new Intent();
        intent.putExtra("data_return","这是第一个活动");
        setResult(RESULT_OK,intent);
        finish();
    }

    /* singleTask启动模式中的onDestroy()方法*/
    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d("SecondActivity", "onDestroy");
    }

    /* 通过actionStart（）方法传递数据*/
    public static void actionStart(Context context,String data1,String data2){
        Intent intent = new Intent(context,SecondActivity.class);
        intent.putExtra("param1",data1);
        intent.putExtra("param2",data2);
        context.startActivity(intent);
    }

}