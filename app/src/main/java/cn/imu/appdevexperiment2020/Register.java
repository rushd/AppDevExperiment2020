package cn.imu.appdevexperiment2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity implements View.OnClickListener {

    private Database myDatabaseHelper;
    private Button btnRegister,btnBack;
    private EditText etAccount, etPass, etSex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        etAccount = findViewById(R.id.register_username);
        etPass=findViewById(R.id.register_password);
        etSex=findViewById(R.id.register_sex);
        btnRegister=findViewById(R.id.register2_button);
        btnBack=findViewById(R.id.back_button);
        btnRegister.setOnClickListener(this);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register.this,SmallQQ_MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        myDatabaseHelper = new Database(Register.this);
    }

    @Override
    public void onClick(View v) {
        String name = etAccount.getText().toString().trim();
        String pass = etPass.getText().toString().trim();
        String sex = etSex.getText().toString().trim();

        if(!TextUtils.isEmpty(name)&&!TextUtils.isEmpty(pass)&&!TextUtils.isEmpty(sex)){
            myDatabaseHelper.add(name,pass,sex);
            Intent intent1 = new Intent(Register.this,SmallQQ_MainActivity.class);
            startActivity(intent1);
            finish();
            Toast.makeText(Register.this,"注册成功",Toast.LENGTH_SHORT).show();
        }else {Toast.makeText(Register.this,"信息不完备，注册失败",Toast.LENGTH_SHORT).show();}
        Toast.makeText(Register.this,"注册成功!",Toast.LENGTH_LONG).show();
        myDatabaseHelper.add(name,pass,sex);
        Intent intent = new Intent(Register.this,SmallQQ_MainActivity.class);
        startActivity(intent);
        finish();
    }

}
