package cn.imu.appdevexperiment2020;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class ContextFragment extends Fragment {

    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab, container, false);
        textView = view.findViewById(R.id.content_text);
        Bundle bundle = getArguments();
        if (bundle != null) {
            String textValue = bundle.getString("textValue");
            textView.setText(textValue);
        }
        return view;
    }
}
