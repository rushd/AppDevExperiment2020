# 《移动应用开发》课程的实验项目

#### 介绍
- 用于专硕《移动应用开发》课程的实验项目
- 提交每一次作业的实现过程和结果


#### 实验目录
1.活动
 1.1 完成环境安装，从码云下载代码
 1.2 完成“Hello 学生名字”应用
 1.3 实现Toast用法以及创建menu菜单
 1.4 显式与隐式Intent
 1.5 传递数据与返回数据
 1.6 活动的生命周期
 1.7 活动的四个启动模式
 1.8 活动的最佳实践（知晓当前活动、随时随地退出程序、启动活动的最佳写法）
2.UI
 2.1 TextView/Button/EditText 控件
 2.2 ImageView/ProgressBar/AlertDialog/ProgressDialog 控件
 2.3 线性布局/相对布局/帧布局/百分比布局
 2.4 ListView 控件
 2.5 RecyclerView 控件
3.碎片
 3.1 静动态添加碎片、体验碎片生命周期、限定符的使用
 3.2 简易版新闻应用
 3.3 基于碎片的QQ导航栏
4.QQ登录应用
 4.1 QQ登录界面
 4.2 QQ注册界面